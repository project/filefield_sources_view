<?php

/**
 * @file
 * A FileField extension using a view to filter the referenceable files list.
 *
 * Like the reference extension, this extension allows reuse of existing files
 * but filters the list using a view. The view may apply additional filter
 * conditions, e.g. based on the entity bundle or other contextual values.
 *
 * The "hooks" in this file are not true hooks; some are called individually
 * from the main filefield_sources.module in the corresponding hook by the
 * same name, some from this module, and some as callbacks defined in the info
 * hook below.
 *
 * The routines in this file are named according to the pattern used for sources
 * by filefield_sources module, namely "filefield_source_{$source}_{$hook}".
 */

/**
 * Implements hook_filefield_source_info().
 */
function filefield_source_view_info() {
  $source['view'] = array(
    'name' => t('Autocomplete textfield (using a view to filter the list of referenceable files)'),
    'label' => t('Reference existing (view)'),
    'description' => t('Reuse an existing file by entering a portion of its name.'),
    'process' => 'filefield_source_view_element_process',
    'value' => 'filefield_source_view_value',
    'weight' => 1,
    'file' => 'sources/view.inc',
  );
  return $source;
}

/**
 * Implements hook_menu().
 */
function filefield_source_view_menu() {
  // @todo If the filefield_sources module is patched to fully implement an API
  // for file field sources, then the next sentence applies.
  // The 'file path' key is required on all implementing modules; otherwise the
  // filefield_sources module will be used as it returns the menu items for the
  // implementing modules.
  $items['file/view/%/%/%'] = array(
    'page callback' => 'filefield_source_view_autocomplete',
    'page arguments' => array(2, 3, 4),
    'access callback' => '_filefield_sources_field_access',
    'access arguments' => array(2, 3, 4),
    'file' => 'sources/view.inc',
    'file path' => drupal_get_path('module', 'filefield_sources_view'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implements hook_theme().
 */
function filefield_source_view_theme() {
  // @todo If the filefield_sources module is patched to fully implement an API
  // for file field sources, then the next sentence applies.
  // The 'path' key is required on all implementing modules; otherwise the
  // filefield_sources module will be used as it returns the theme items for the
  // implementing modules.
  return array(
    'filefield_source_view_element' => array(
      'render element' => 'element',
      'file' => 'sources/view.inc',
      'path' => drupal_get_path('module', 'filefield_sources_view'),
    ),
    'filefield_source_view_autocomplete_item' => array(
      'variables' => array('file' => NULL),
      'file' => 'sources/view.inc',
      'path' => drupal_get_path('module', 'filefield_sources_view'),
    ),
 );
}

/**
 * Implements hook_filefield_source_settings().
 *
 * @todo
 * The form elements are in the 'source_reference' group as this module extends
 * the reference source. Normally, they would be in a 'source_view' group as
 * that is the short name of this source.
 *
 * This routine is not called as filefield_sources only invokes it on the
 * pseudo-modules defined in its sources directory. It is here for illustration
 * if filefield_sources were to implement a more complete API.
 */
function filefield_source_view_settings($op, $instance) {
  $return = array();

  if ($op == 'form') {
    // @todo This and the form_alter should not both run.
    return $return;
    $return = filefield_source_view_form($instance);
    return $return;
  }
  elseif ($op == 'save') {
    $return['source_reference']['view'] = '';
    $return['source_reference']['args'] = '';
  }

  return $return;
}

/**
 * Form constructor for the field settings form.
 *
 * @todo Unlike other form constructor routines this is not passed $form and
 * $form_state in the filefield_sources API; mimic that limitation here.
 */
function filefield_source_view_form($instance/*, &$form_state*/) {
  // Gather data.
  $settings = $instance['widget']['settings']['filefield_sources'];
  $states = array(
    'invisible' => array(
      ':input[name="instance[widget][settings][filefield_sources][filefield_sources][view]"]' => array('checked' => FALSE),
    ),
  );

  // Define form elements.
  $options = filefield_source_view_list();
  $default = !empty($settings['source_reference']['view']) ? $settings['source_reference']['view'] : NULL;
  $elements['source_reference']['view'] = array(
    '#type' => 'select',
    '#title' => t('View used to filter the files'),
    '#options' => $options,
    '#default_value' => $default,
    '#description' => t('The view and display that returns a list of files that may be referenced. Only views of type "File" are included.'),
    '#weight' => 1,
    '#states' => $states,
    '#element_validate' => array('filefield_source_view_form_validate'),
  );

  $default = !empty($settings['source_reference']['args']) ? implode(', ', $settings['source_reference']['args']) : '';
  $elements['source_reference']['args'] = array(
    '#type' => 'textfield',
    '#title' => t('View arguments'),
    '#default_value' => $default,
    '#required' => FALSE,
    '#description' => t('A comma separated list of arguments to pass to the view.'),
    '#weight' => 2,
    '#states' => $states,
  );

  return $elements;
}

/**
 * Element validation handler for the field settings form.
 */
function filefield_source_view_form_validate($element, &$form_state, $form) {
  $sources = $form_state['values']['instance']['widget']['settings']['filefield_sources']['filefield_sources'];
  if ($sources['view'] && empty($element['#value'])) {
    form_error($element, t('The "@field" field is required.', array('@field' => $element['#title'])));
  }
}

/**
 * Returns an associative list of views and display IDs.
 *
 * Eligible views have 'file_managed' as the base table (indicated as 'File'
 * type in the views listing page). The returned array is suitable as an options
 * array for select, checkboxes and radios form elements.
 *
 * @return array
 *   An associative array of view name and display title keyed by view name and
 *   display ID.
 */
function filefield_source_view_list() {
  $options = array('' => '- Select -');
  $views = views_get_all_views();
  foreach ($views as $view) {
    if ($view->base_table == 'file_managed' && !empty($view->display['default']->display_options['fields'])) {
      foreach ($view->display as $display_id => $display) {
        $options[$view->name . ':' . $display_id] = $view->name . ' - ' . $view->display[$display_id]->display_title;
      }
    }
  }
  ksort($options);
  return $options;
}

/**
 * Implements callback_filefield_source_info_process().
 *
 * A '#process' callback to the managed_file element type.
 *
 * Add '_element' to routine name to avoid conflict with hook_process().
 */
function filefield_source_view_element_process($element, &$form_state, $form) {
  $element['filefield_view'] = array(
    '#weight' => 100.5,
    '#theme' => 'filefield_source_view_element',
    '#filefield_source' => TRUE, // Required for proper theming.
    '#filefield_sources_hint_text' => FILEFIELD_SOURCE_REFERENCE_HINT_TEXT,
  );

  $element['filefield_view']['autocomplete'] = array(
    '#type' => 'textfield',
    '#autocomplete_path' => 'file/view/' . $element['#entity_type'] . '/' . $element['#bundle'] . '/' . $element['#field_name'],
    '#description' => filefield_sources_element_validation_help($element['#upload_validators']),
  );

  $element['filefield_view']['select'] = array(
    '#name' => implode('_', $element['#array_parents']) . '_autocomplete_select',
    '#type' => 'submit',
    '#value' => t('Select'),
    '#validate' => array(),
    '#submit' => array('filefield_sources_field_submit'),
    '#name' => $element['#name'] . '[filefield_view][button]',
    '#limit_validation_errors' => array($element['#parents']),
    '#ajax' => array(
      'path' => 'file/ajax/' . implode('/', $element['#array_parents']) . '/' . $form['form_build_id']['#value'],
      'wrapper' => $element['upload_button']['#ajax']['wrapper'],
      'effect' => 'fade',
    ),
  );

  return $element;
}

/**
 * Implements callback_filefield_source_info_view().
 *
 * A '#filefield_value_callbacks' callback to the managed_file element type.
 *
 * @todo file_file_download($uri, $field_type = 'file') will deny access if the
 * file is not referenced on a field of same type (not bundle but field type).
 */
function filefield_source_view_value($element, &$item) {
  if (isset($item['filefield_view'])) {
    // Call corresponding routine in filefield_sources.
    $item['filefield_reference'] = $item['filefield_view'];
    filefield_source_reference_value($element, $item);
  }
}

/**
 * Page callback: Returns a list of files.
 *
 * Invoked by autocomplete.js on a textfield form element.
 */
function filefield_source_view_autocomplete($entity_type, $bundle_name, $field_name, $filename) {
  // @todo Rename to $instance (here and in filefield_sources).
  $instance = field_info_instance($entity_type, $field_name, $bundle_name);

  $items = array();
  if (!empty($instance)) {
    $files = filefield_source_view_files($filename, $instance);
    foreach ($files as $fid => $file) {
      $items["$file->file_managed_filename [fid:$fid]"] = theme('filefield_source_view_autocomplete_item', array('file' => $file));
    }
  }

  drupal_json_output($items);
}

/**
 * Returns HTML for a single item in the autocomplete list.
 *
 * @param array $variables
 *   An associative array containing:
 *   - file: The file object that is being formatted.
 *
 * @ingroup themeable
 */
function theme_filefield_source_view_autocomplete_item($variables) {
  $file = $variables['file'];

  $output = '';
  $output .= '<div class="filefield-source-view-item">';
//   $output .= '<span class="filename">' . check_plain($file->filename) . '</span> <span class="filesize">(' . format_size($file->filesize) . ')</span>';
  $output .= $file->rendered;
  $output .= '</div>';
  return $output;
}

/**
 * Returns HTML for the autocomplete form element.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: The form element that is being formatted.
 *
 * @ingroup themeable
 */
function theme_filefield_source_view_element($variables) {
  $element = $variables['element'];

  $element['autocomplete']['#field_suffix'] = drupal_render($element['select']);
  return '<div class="filefield-source filefield-source-view clear-block">' . drupal_render($element['autocomplete']) . '</div>';
}

/**
 * Returns a list of referenceable files defined by a view.
 *
 * The view is specified in the instance settings.
 * The minimal view requirements are a fid column and a filename filter.
 *
 * @param $match
 *   The partial name of the file(s) to retrieve.
 * @param $instance
 *   (optional) A field instance array for which to filter returned files.
 *
 * @return array
 *   Associative array of referenceable files keyed by file ID.
 */
function filefield_source_view_files($match, $instance = NULL) {
  $files = array();
  $settings = $instance['widget']['settings']['filefield_sources'];
  if ($settings['source_reference']['view'] == '') {
    return $files;
  }

  // Referenceable files are defined in a view.
  // @see EntityReference_SelectionHandler_Views::initializeView()
  // 1 == contains, 0 == starts with.
  // This form element is defined by reference.inc in filefield_sources.
  $match_operator = empty($settings['source_reference']['autocomplete']) ? 'starts' : 'contains';
  $files = array();
  $settings = $instance['widget']['settings']['filefield_sources'];
  list($view_name, $display_id) = explode(':', $settings['source_reference']['view']);

  $view = views_get_view($view_name);
  if (empty($view)) {
    return $files;
  }

  $view->set_display($display_id);

  // Limit result set size.
  $limit = isset($limit) ? $limit : 10;
  $view->set_items_per_page($limit);

  $fields = $view->get_items('field', $display_id);
  if (!isset($fields['fid'])) {
    // Add fid field to the view; set to not display.
    $id = $view->add_item($display_id, 'field', 'file_managed', 'fid');
    $item = $view->get_item($display_id, 'field', $id);
    $item['exclude'] = TRUE;
    $view->set_item($display_id, 'field', $id, $item);
  }

  $options = $view->display_handler->get_option('row_options');
  $options['default_field_elements'] = FALSE;
  // Inline display all fields.
  $options['inline'] = drupal_map_assoc(array_keys($view->get_items('field', $display_id)));
  if (empty($options['separator'])) {
    // Define a separator for inline fields.
    $options['separator'] = '-';
  }
  $view->display_handler->set_option('row_options', $options);

  // Add a filename filter to use with autocomplete input.
  // The view display may define a similar filter with other conditions.
  $id = $view->add_item($display_id, 'filter', 'file_managed', 'filename');
  $item = $view->get_item($display_id, 'filter', $id);
  $item['operator'] = $match_operator;
  $item['value'] = $match;
  $view->set_item($display_id, 'filter', $id, $item);

  // Make sure the query is not cached.
  // @todo Is this done by entityreference? Do we need it? User can set it.
  $view->is_cacheable = FALSE;

  // Get the results.
  $output = $view->execute_display($display_id);
  $results = $view->result;
  $row_plugin = $view->style_plugin->row_plugin;

  // @todo Here $result is $file, sort of.
  foreach ($results as $index => $result) {
//     foreach ($result as $property => $value) {
//       if (strpos($property, 'file_managed_') === 0) {
//         $new_property = substr($property, 13);
//         $result->$new_property = $value;
//         unset($result->$property);
//       }
//     }
    $view->row_index = $index;
    // Sanitize html, remove line breaks and extra whitespace.
    $result->rendered = filter_xss_admin(preg_replace('/\s\s+/', ' ', str_replace("\n", '', $row_plugin->render($result))));
    // Add file to list.
    $files[$result->fid] = $result;
  }

  return $files;
}
